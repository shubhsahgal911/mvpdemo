# MVP Demo #

1. A .gitIgnore file is added to avoid any unnecessay commits. Follow : (https://github.com/codepath/ios_guides/wiki/Using-Git-with-Terminal)
2. This project consist of two configurations Debug and Release. The Base Url, Facebook ID etc has been defined as the user defined attributes. It can be easily used using struct mentioned in AppConstants.
     For more information follow : https://medium.com/@guyeldar/setting-up-multiple-build-configurations-for-your-xcode-project-c237265e8324
3. All the String Literals need to be defined in AppConstant.swift or as Enums.
4. Only UI Changes should be made in view controller.
5. Every ViewController will have a Presenter and Model.
6. Use single pattern for variable and Outlet. Like for UI elements prefix the type followed by the name.Example : textFieldEmail, tableViewUsers.
7. Please do check all the extension. Use them instead of re-writing the code. Kindly go through the complete project before start so there is no duplicate code.


### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
