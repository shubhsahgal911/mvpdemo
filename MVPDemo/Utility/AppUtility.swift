//
//  AppUtility.swift
//  This class is used to write code which is very frequently used through out the project. Create static methods only.
//

import Foundation
import UIKit
class AppUtility{
    
    /// Check if user is login or not
    ///
    /// - Returns: User login status
    class func isUserLogin()->Bool{
        return checkKeyInUserDefault(key: AppConstants.UserDefaultKeys.IS_ALREADY_LOGIN)
    }
    
    /**
     * This method is used for checking particular in User Default
     * parameter : String Object
     * @returns  : Bool
     */
    
    class func checkKeyInUserDefault(key:String) -> Bool {
        if(UserDefaultUtility.retrieveBoolForKey(key) == true) {
            
            if (UserDefaultUtility.objectAlreadyExist(key) == true){
                return true
            }
            return false
        }
        return false
    }
  
    /// Format date with given format
    ///
    /// - Parameters:
    ///   - format: string date format
    ///   - date: Date to be formatted
    /// - Returns: Formatted date string
    class func getFormattedDate(withFormat format: String, date: Date)->String{
        let dateFormatter = DateFormatter()
        //Set Locale
        let enUSPOSIXLocale: Locale = Locale(identifier:"en_US_POSIX")
        dateFormatter.locale = enUSPOSIXLocale
        //Set Formatter
        dateFormatter.dateFormat = format
        //Get Formatted Date
        let dateInString = dateFormatter.string(from: date)
        return dateInString
    }    
    
    class func openAppUrl(appUrl:String,webUrl:String) {
        if let appURL:URL = URL(string: appUrl) {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(appURL as URL)) {
                application.open(appURL, options: [:], completionHandler: nil)
            }
            else
            {
                application.open(URL(string: webUrl)!, options: [:], completionHandler: nil)
            }
        }
    }
}
