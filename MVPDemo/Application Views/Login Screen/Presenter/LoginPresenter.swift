//
//  LoginScreenPresenter.swift
//

import Foundation

class LoginScreenPresenter: ResponseCallback{
    
    //MARK:- LoginScreenPresenter local properties
    private weak var loginViewDelegate             : LoginViewDelegate?
    private weak var textFieldValidationDelegate   : TextFieldValidationDelegate?
    
    //MARK:- Constructor
    init(delegate loginDelegate: LoginViewDelegate) {
        self.loginViewDelegate = loginDelegate
    }
    
    convenience init(delegate responseDelegate:LoginViewDelegate,textFieldValidationDelegate: TextFieldValidationDelegate) {
        self.init(delegate: responseDelegate)
        self.textFieldValidationDelegate = textFieldValidationDelegate
    }
    
    
    //MARK:- ResponseCallback delegate methods
    
    func servicesManagerSuccessResponse<T:Codable> (responseObject : T){
        UserDefaultUtility.saveBoolForKey(AppConstants.UserDefaultKeys.IS_ALREADY_LOGIN, value: true)
        self.loginViewDelegate?.hideLoader()
        if let responseModel =  responseObject as? LoginResponseModel
        {
            self.loginViewDelegate?.loginSuccessfull(withResponseModel: responseModel)
        }
    }
    
    func servicesManagerError(error : ErrorResponse){
        self.loginViewDelegate?.hideLoader()
        self.loginViewDelegate?.showErrorAlert(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: error.status.error ?? "")
    }
    
    
    //MARK:- Methods to call server
    
    /**
     This method is used to send login request to business layer with valid Request model
     - returns : Void
     */
    func sendLoginRequest(withData loginViewRequestModel:LoginScreenRequestModel) -> Void{
        
        guard self.validateInput(withData: loginViewRequestModel) else{
            return
        }
        
        self.loginViewDelegate?.showLoader()
        
        let loginRequestModel = LoginRequestModel.Builder()
            .setUserName(loginViewRequestModel.email)
            .setPassword(loginViewRequestModel.password)
            .addRequestHeader(key: AppConstants.APIRequestHeaders.CONTENT_TYPE, value: AppConstants.APIRequestHeaders.APPLICATION_JSON)
            .build()
        let apiRequestUrl = AppConstants.URL.BASE_URL+loginRequestModel.getEndPoint()
        
        let responseWrapper = ResponseWrapper(responseCallBack: self)
        
        ServiceManager.sharedInstance.requestPOSTWithURL(apiRequestUrl, andRequestDictionary: loginRequestModel.requestBody, requestHeader: loginRequestModel.requestHeader, responseCallBack: responseWrapper, returningClass: LoginResponseModel.self)
    }
    
    //MARK:- Methods to validate input
    
    /**
     Validates input fields for login view request model(PreLogin Required Action).
     Also show alert on the view with proper message if not valid in any case.
     - parameter loginRequestModel: LoginScreenRequestModel recieved from Login view controlller
     - returns : whether the input are valid or not
     */
    func validateInput(withData loginRequestModel:LoginScreenRequestModel) -> Bool {
        
        // All fields not empty
        guard self.validateAllFieldsEmpty(withData: loginRequestModel) else{
            //All fields empty
            self.textFieldValidationDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.ENTER_EMAILID, forTextFields: .All)
            return false
        }
        
        //email validation
        guard !loginRequestModel.email.isEmptyString() else{
            //email or phone empty
            self.textFieldValidationDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.ENTER_EMAILID, forTextFields: .EmailOrPhone)
            return false
        }
        
        guard loginRequestModel.email.isValidEmailId() else {
            //email or phone not valid
            self.textFieldValidationDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.ENTER_VALID_EMAILID, forTextFields: .EmailOrPhone)
            return false
        }
        
        //Password validaiton
        guard !loginRequestModel.password.isEmpty else {
            //password empty
            self.textFieldValidationDelegate!.showErrorMessage(withMessage: AppConstants.ValidationErrors.ENTER_PASSWORD, forTextFields: .Password)
            return false
        }
        
        return true
    }
    
    /**
     Validate that all fields are empty or not
     - parameter loginRequestModel : LoginScreenRequestModel
     */
    func validateAllFieldsEmpty(withData loginRequestModel:LoginScreenRequestModel) -> Bool{
        guard !loginRequestModel.email.isEmptyString() || !loginRequestModel.password.isEmpty else {
            return false
        }
        return true
    }
}
