//
//  LoginScreenViewController.swift
//

import UIKit


class LoginScreenViewController: BaseViewController {

    //MARK:- LoginViewController local properties.
    var loginPresenter : LoginScreenPresenter!
   
    //MARK:- IBOutlets
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var textFieldEmail: UITextField!
    
    //MARK:- View Controller Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.intialSetupForView()
        self.loginPresenter = LoginScreenPresenter(delegate: self,textFieldValidationDelegate: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- IBAction for Buttons
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        self.loginPresenter.sendLoginRequest(withData: self.createLoginScreenRequestModelWithTextFieldInput())
    }
    
    //MARK:- Helper methods
    
    /**
     This method is used for initial setups
     */
     private func intialSetupForView(){
        self.setupNavigationBar()
    }
    
    /**
     This method is used to setup navigationBar
     */
    private func setupNavigationBar(){
//        self.customizeNavigationBarWithTitle(navigationTitle: "")
//        self.customizeNavigationBackButton()
    }

    /**
     This method is used to get the loginViewRequestModel from the current input in text fields
     
     -returns : EmailLoginViewRequestModel with current data in textFields
     */
    func createLoginScreenRequestModelWithTextFieldInput() -> LoginScreenRequestModel {
        var loginScreenRequestModel = LoginScreenRequestModel()
        loginScreenRequestModel.email = self.textFieldEmail.text?.getWhitespaceTrimmedString()
        loginScreenRequestModel.password = self.textFieldPassword.text
        return loginScreenRequestModel
    }

    
    // MARK: - Navigation
    
    func navigateToViewController(){
        
        //Replace LoginScreenViewController.self with destination view controller.
//       let viewController = UIViewController.getViewController(LoginScreenViewController.self, storyboard: UIStoryboard.Storyboard.Main.object)
//        self.navigationController?.pushViewController(viewController, animated: true)
    }
}


// MARK: - LoginViewDelegate Methods
extension LoginScreenViewController : LoginViewDelegate
{
    func showLoader(){
        self.showLoader(self)
    }
    
    func hideLoader(){
        self.hideLoader(self)
    }
    
    func showErrorAlert(_ alertTitle : String , alertMessage : String){
        self.showErrorAlert(alertTitle, alertMessage: alertMessage, VC: self)
    }
    func loginSuccessfull(withResponseModel loginResponseModel:LoginResponseModel)
    {
        UserDefaultUtility.saveStringWithKey(loginResponseModel.token, key: AppConstants.UserDefaultKeys.USER_TOKEN)
        self.navigateToViewController()
    }
    
}

// MARK: - TextFieldValidationDelegate Methods
extension LoginScreenViewController : TextFieldValidationDelegate
{
    func showErrorMessage(withMessage message: String,forTextFields: TextFieldsType) {
         self.showErrorAlert(AppConstants.ErrorMessages.ALERT_TITLE, alertMessage: message)
    }
}

