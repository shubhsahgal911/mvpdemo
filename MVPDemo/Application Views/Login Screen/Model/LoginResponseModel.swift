//
//  LoginResponseModel.swift
//

import Foundation


// To parse the JSON, add this file to your project and do:
//
//   let loginResponseModel = try? newJSONDecoder().decode(LoginResponseModel.self, from: jsonData)

import Foundation

struct LoginResponseModel: Codable {
    let status: Status?
    let data: DataClass
    let token: String
}

struct DataClass: Codable {
    let id: Int
    let username, firstName, lastName, email: String
    let isActive: Bool
    let permissionGroups: [PermissionGroup]
    let street, city, state, zipcode: String
    let address, tz: String
    let userphoneSet: [String]
    //let dealeruser: String
   // let vehicleSet: [String]
    
    enum CodingKeys: String, CodingKey {
        case id, username
        case firstName = "first_name"
        case lastName = "last_name"
        case email
        case isActive = "is_active"
        case permissionGroups = "permission_groups"
        case street, city, state, zipcode, address, tz
        case userphoneSet = "userphone_set"
//        case dealeruser
//        case vehicleSet = "vehicle_set"
    }
}

struct PermissionGroup: Codable {
    let id: Int
    let createdOn, modifiedOn, name: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case createdOn = "created_on"
        case modifiedOn = "modified_on"
        case name
    }
}

struct Status: Codable {
    let code: Int
    let message, error: String?
}

