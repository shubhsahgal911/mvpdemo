//
//  ErrorResponse.swift
//
//  Created by  on 26/02/18
//  Copyright (c) . All rights reserved.
//

import Foundation


struct ErrorResponse: Codable {
    let status: Status
}
