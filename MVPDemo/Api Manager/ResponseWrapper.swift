
import Foundation
protocol ApiResponseReceiver{
    func onSuccess<T: Codable>(_ response:T) -> Void
    func onError(_ errorResponse:ErrorResponse) -> Void
}

class ResponseWrapper : ApiResponseReceiver  {
    
    let delegate         : ResponseCallback!
   
    init(responseCallBack:ResponseCallback){
        self.delegate = responseCallBack
    }
    
    /**
     This method is used for handling Success response of an API
     
     - parameter response: Response is a kind of Generic Object
     */
    
    func onSuccess<T:Codable>(_ response:T) -> Void {
        self.delegate.servicesManagerSuccessResponse(responseObject: response)
    }
    
    /**
     This method is used for handling Error response of an API
     
     - parameter errorResponse: NSError Object contains error info
     */
    
    func onError(_ errorResponse:ErrorResponse) ->Void {
        
        self.delegate.servicesManagerError(error : errorResponse)
    }
    
}

