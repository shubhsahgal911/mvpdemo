//
//  UIApplicationExtension.swift
//
import UIKit
extension UIApplication {
    
    var visibleViewController: UIViewController? {
        
        guard let rootViewController = keyWindow?.rootViewController else {
            return nil
        }
        
        return getVisibleViewController(rootViewController)
    }
    var rootViewController: UIViewController? {
        
        guard let _ = keyWindow?.rootViewController else {
            return nil
        }
        
        return keyWindow?.rootViewController
    }
    
    private func getVisibleViewController(_ rootViewController: UIViewController) -> UIViewController? {
        
        if let presentedViewController = rootViewController.presentedViewController {
            return getVisibleViewController(presentedViewController)
        }
        
        if let navigationController = rootViewController as? UINavigationController {
            return navigationController.visibleViewController
        }
        return rootViewController
    }
}
