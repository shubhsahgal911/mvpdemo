//
//  DateExtension.swift
//

import UIKit
import Foundation

extension Date{
    func getUTCFormateDate() -> String {
        let dateFormatter = DateFormatter()
        let timeZone = TimeZone(identifier: "UTC")
        dateFormatter.timeZone = timeZone
        dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
        let dateString: String = dateFormatter.string(from: self)
        return dateString
    }
}
