
//Note:- This class contains color code of specific colors

import UIKit


extension UIColor{
    
    static func appThemeColor() -> UIColor {
        return UIColor(red: 165/255.0, green: 0/255.0, blue: 80/255.0, alpha: 1.0)
    }
}
