
//Note :- This method is used for Handling Api Response


protocol ResponseCallbackGeneral:class {
    func servicesManagerSuccessResponse<T:AnyObject>(responseObject : T)
    func servicesManagerError(error : ErrorResponse)
}

