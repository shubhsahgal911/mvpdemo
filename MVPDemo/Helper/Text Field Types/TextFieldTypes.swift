//
//  TextFieldTypes.swift
//

import Foundation
enum TextFieldsType{
    case EmailOrPhone
    case EmailAddress
    case PhoneNumber
    case Password
    case FirstName
    case LastName
    case OTP
    case OldPassword
    case NewPassword
    case CurrentPassword
    case Indiviual
    case Partnership
    case All  //all text fields for current view
}
