
//
//  APNSManager.swift
//

import UIKit
import Foundation
import UserNotifications
enum PUSH_EVENTS : String
{
    case EVT_NEW                = "EVT_NEW"
}

enum TRIP_STATUS : String
{
    case STARTED                = "STARTED"
    case CONFIRMED              = "CONFIRMED"
    case CANCELLED              = "CANCELLED"
    case ENDED                  = "ENDED"
    case EXPIRED                = "EXPIRED"
}
class APNSManager: NSObject{
    
    static let sharedInstance = APNSManager()
    
    private override init(){}
    
    
    /// This method is used to register app for recieving push notification with settings
    func registerForPushNotification(){
        
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in
            // Enable or disable features based on authorization.
        }
        
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    
    /// This method is used to handle notification payload if user launch app from backgroud or from new instance
    ///
    /// - Parameter options: launch options dictionary
    func handleNotificationPayloadOnLaunch(withLaunchOptions options: [UIApplicationLaunchOptionsKey: Any]?){
        if let dict = options?[UIApplicationLaunchOptionsKey.remoteNotification]{
            self.onRecievingPushNotification(withPayload: dict as! NSDictionary)
        }
    }
    
    
    /// This method is called when device is successfully registred with APNS server and server responds with device token for the app
    ///
    /// - Parameter withDeviceToken: Device token in string format
    func onRegistrationSuccess(withDeviceToken deviceToken: String){
        UserDefaultUtility.saveStringWithKey(deviceToken, key: AppConstants.UserDefaultKeys.DEVICE_TOKEN)
    }
    
    
    /// This method is called when device recieve any push notifcation from APNS server
    ///
    /// - Parameter data: data representing payload of recieved notification
    func onRecievingPushNotification(withPayload data: NSDictionary){
        
    }
    
//    func handleNotificationEvent(_ objNotification:PushNotificationObjectModel)
//    {
////        switch objNotification.eventName! {
////        }
//    }
}
