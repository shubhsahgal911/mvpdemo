//
//  LocationServiceManager.swift
//
//
import Foundation
import CoreLocation
import UIKit
protocol LocationUpdateDelegate: class {
    func showControllerWithVC(controller: UIViewController)
    func locationUpdated(lat: Double, long: Double)
}


extension LocationUpdateDelegate
{
    func receivedAddressFromLocation(_ address:String){}
    func showControllerWithVC(controller: UIViewController){}
}
class LocationServiceManager: NSObject, CLLocationManagerDelegate{
    
    static let sharedInstance = LocationServiceManager()
    var currentLocation: CLLocation?
 
    weak var viewDelegate: LocationUpdateDelegate?
    
    private var manager = CLLocationManager()

    private func configureLocationServices(){
        
        self.checkForUserPermissions()
    }
    
    //MARK: Location Manager Delegate
    private func locationManager(manager: CLLocationManager,
                         didChangeAuthorizationStatus status: CLAuthorizationStatus)
    {
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            manager.startUpdatingLocation()
            manager.delegate = self
        }
        else{
            self.checkForUserPermissions()
        }
    }
    
    func checkForUserPermissions(){
        manager.delegate = self
        
        switch CLLocationManager.authorizationStatus()
        {
        case .authorizedAlways:
        manager.startUpdatingLocation()
            break
        case .notDetermined:
            manager.requestAlwaysAuthorization()
        case .authorizedWhenInUse:
        manager.startUpdatingLocation()
            break
        case .restricted, .denied:
            let alertController = UIAlertController(
                title: "AppConstants.ScreenSpecificConstant.WelcomeScreen.TITLE_LOCATION_ACCESS_DISABLED",
                message: "AppConstants.ScreenSpecificConstant.WelcomeScreen.MESSAGE_LOCATION_SERVICE_DISABLED",
                preferredStyle: .alert)
            
            let openAction = UIAlertAction(title: "Open Settings", style: .default) { (action) in
                if let url = NSURL(string:UIApplicationOpenSettingsURLString) {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
                    } else {
                        // Fallback on earlier versions
                        
                    }
                }
            }
            alertController.addAction(openAction)
            self.viewDelegate!.showControllerWithVC(controller: alertController)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
       self.currentLocation = locations.last
        self.viewDelegate?.locationUpdated(lat: (self.currentLocation?.coordinate.latitude)!, long: (self.currentLocation?.coordinate.longitude)!)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
    }
}
