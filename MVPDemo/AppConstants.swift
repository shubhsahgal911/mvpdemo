/**
 Contains all the constants related to the application
 */

import Foundation

struct AppConstants{
    
    struct PListKeys{

        static let BASE_URL_KEY = "Base URL"
        static let CLIENT_ID_KEY = "ClientID"
        static let CLIENT_SECRET_KEY = "ClientSecret"
        static let TWITTER_KEY = "TwitterKey"
        static let TWITTER_SECRET_KEY = "TwitterSecret"
    }
    //MARK:This structure contains Api URLs
    
    struct URL{
        static let BASE_URL:String = PListUtility.getValue(forKey: AppConstants.PListKeys.BASE_URL_KEY) as! String
    }
    
    //MARK:This structure contains Api End Points
    
    struct ApiEndPoints {
         static let LOGIN = "login/"
    }
    
    
    //MARK:- This structure contains Error message for various events
    
    struct ValidationErrors{
        static let ENTER_PASSWORD = "Please enter the password."
        static let ENTER_EMAILID = "Please enter email id."
        static let ENTER_VALID_EMAILID = "Please enter a valid email id."
        
    }
    
    //MARK:- This structure contains Request header keys
    
    struct APIRequestHeaders {
        static let CONTENT_TYPE = "Content-Type"
        static let APPLICATION_JSON = "application/json"
        static let API_KEY = "api_key"
        static let AUTH_TOKEN = "Authorization"
    }
    
    //MARK:- This structure contains keys for error handling
    
    struct ErrorHandlingKeys{
        
        static let IS_ERROR = ""
        static let ERROR_KEY = "code"
        static let ERROR_DETAIL = "detail"
    }
    
    //MARK:- This structure contains the error messages corresponding to error code
    
    struct ErrorMessages{
        static let ALERT_TITLE = "Alert"
        static let MESSAGE_TITLE = "Message"
        static let INVALID_KEY_MESSAGE = "Valid api key is required. Please provide a valid api key along with request"
        static let SOME_ERROR_OCCURED = "SOME_ERROR_OCCURED"
        static let REQUEST_TIME_OUT = ""
        static let PLEASE_CHECK_YOUR_INTERNET_CONNECTION = "Please check your internet connection."
        //Direction API
        static let BAD_REQUEST    = "Unable to show direction for this path. Please try different locations"
        static let UNAUTHORIZED   = "You are not authorized to access this functionality"
        static let METHOD_NOT_ALLOWED = "The specified HTTP method is not supported."
        static let LARGE_REQUEST = "The request is larger than the server is able to process, the data provided in the request exceeds the capacity limit."
        static let INTERNAL_SERVER_ERROR = "The request is incorrect and therefore can not be processed."
        static let FACILITY_NOT_SUPPORTED = "The server does not support the functionality needed to fulfill the request."
        static let SERVER_OVERLOAD = "The server is currently unavailable due to overload or maintenance."
    }
    
    struct ScreenSpecificConstant {
        struct Common{
            static let BACK_BUTTON_TITLE = "BACK_BUTTON_TITLE"
            static let SKIP_BUTTON_TITLE = "SKIP_BUTTON_TITLE"
            static let EMPTY_STRING = ""
            static let LOGOUT_TITLE = ""
            static let YES_TITLE = "YES_TITLE"
            static let OK_TITLE = "Ok"
            static let NO_TITLE = "NO_TITLE"
            static let Cancel_TITLE = "Cancel"
            static let LOGOUT_MESSAGE = "Are you sure you want to logout?"
            static let SETTINGS = "Go To Settings"
            static let ACCESS_DENIED = "Access Denied"
            static let CONTACT_ACCESS_PERMISSION_MESSAGE = "Please allow the app to access your contacts through the Settings"
            static let SAVE_BUTTON_TITLE = "Save"
            static let EDIT_BUTTON_TITLE = "Edit"
        }
        struct Login {
   
        }
    }
    
    
    //MARK:- Common constants

    
    //MARK:- NSNotification Names
    struct NSNotificationNames {
        static let APP_BECOME_ACTIVE_NOTIFICATION = "APP_BECOME_ACTIVE_NOTIFICATION"
    }

    //MARK: Persistent user default data keys
    
    struct UserDefaultKeys{
        static let IS_ALREADY_LOGIN = "IS_ALREADY_LOGIN"
        static let DEVICE_TOKEN = "DEVICE_TOKEN"
         static let USER_TOKEN = "USER_TOKEN"
    }
    
    //MARK:- Date Formatter constants
    
    struct DateConstants {
        static let DOB_FORMAT = "dd/MM/yyyy"
        static let DOB_FORMAT_FROM_SERVER = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    }
}
